pipeline {
    parameters {
        string(name: 'PERSON', defaultValue: 'Mr Jenkins', description: 'Who should I say hello to?')

        text(name: 'BIOGRAPHY', defaultValue: '', description: 'Enter some information about the person')

        booleanParam(name: 'TOGGLE', defaultValue: true, description: 'Toggle this value')

        choice(name: 'CHOICE', choices: ['One', 'Two', 'Three'], description: 'Pick something')

        password(name: 'PASSWORD', defaultValue: 'SECRET', description: 'Enter a password')
    }
    //triggers {
    //    cron('H */4 * * 1-5')
    //}
    agent none
    options {
        retry(3)
        timeout(time: 1, unit: 'HOURS') 
    }
    stages {
        stage('Agent shell') {
            agent { label 'agent' }
            environment { 
                SECRET = credentials('test_secret') 
                JUST_ADDITIONAL_ENV = "Дополнительные переменные окружения"
            }
            steps {
                sh 'hostname'
                sh 'env'
                sh 'pwd'
                sh 'echo "test" > test.txt'
                stash(name: 'testfile',includes: 'test.txt')
            }
            post{
                success{
                    echo 'Этот блок выполнится если состояние текущей сборки success в рамках этапа Agent shell'
                }
                cleanup{
                    echo 'Этот блок выполнится после выполенния блоков всех post состояний в рамках этапа Agent shell'
                }
            }
        }
        stage('Node') {
            agent {
                node{
                    label 'agent'
                    customWorkspace '/tmp/build'
                }
            }
            steps {
                sh 'hostname'
                sh 'env'
                sh 'pwd'
                sh 'ls -la'
            }
        }
        stage('Docker') {
            agent {
                docker { image 'ubuntu' }
            }
            steps {
                sh 'hostname'
                sh 'env'
                sh 'pwd'
                sh 'ls -la'
            }
        }
        stage('Kubernetes') {
        agent {
                kubernetes {
                    yaml '''
                    apiVersion: v1
                    kind: Pod
                    spec:
                      containers:
                      - name: shell
                        image: ubuntu
                        command:
                        - sleep
                        args:
                        - infinity
                    '''
                    defaultContainer 'shell'
                    retries 2
                }
            }
            input {
                message "Should we continue?"
                ok "Yes, we should."
                parameters {
                    string(name: 'PERSON', defaultValue: 'Mr Jenkins', description: 'Who should I say hello to?')
                }
            }
            steps {
                sh 'hostname'
                sh 'env'
                sh 'pwd'
                unstash(name: 'testfile')
                copyArtifacts(projectName: 'Free style job 1')
                sh 'ls -la'
                sh 'cat ./*.txt > declarative_result_$BUILD_NUMBER.txt'
                archiveArtifacts(artifacts: 'declarative_result_*.txt')
            }
        }
    }
    post {
        always{
            echo 'Этот блок выполнится всегда'
        }
        changed{
            echo 'Этот блок выполнится если изменится состояние сборки по сравнению с предыдущей'
        }
        fixed{
            echo 'Этот блок выполнится если изменится состояние сборки на успешное после неуспешной'
        }
        regression{
            echo 'Этот блок выполнится если изменится состояние сборки на одно их неуспешных состояний после успешной'
        }
        aborted{
            echo 'Этот блок выполнится если сборка будет прервана'
        }
        failure{
            echo 'Этот блок выполнится если состояние текущей сборки failure'
        }
        success{
            echo 'Этот блок выполнится если состояние текущей сборки success'
        }
        unstable{
            echo 'Этот блок выполнится если состояние текущей сборки unstable'
        }
        unsuccessful{
            echo 'Этот блок выполнится если состояние текущей сборки failure, unstable или сборка была прервана'
        }
        cleanup{
            echo 'Этот блок выполнится после выполенния блоков всех post состояний'
        }
    }
}